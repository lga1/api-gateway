# import list 
import random
import requests
import base64
import uuid
import json
import time


class SendRequest:
    def __init__(self):
        self.apiuser= '783e8262-0bf4-4794-a2d6-0cdbce2089c3'
        self.api_key = '9b1a92f84d0b4634a2c7ce86983994ac'
        self.token = None
        subscribtion_key_list=['24f8647f940044e58308db1e792ddd64'] #, '25d7754679f54a01a393122e84700d29']
        self.subscribtion_key=random.choice(subscribtion_key_list)
    
    def get_token(self):
        token = ""
        x_reference_id_bytes = str(self.apiuser) + ":" + str(self.api_key)
        key =  x_reference_id_bytes.encode('ascii')
        authorization = base64.b64encode(key)
        Authorization = authorization.decode('ascii')

            # /token - POST
            # This operation is used to create an access token which can then be 
            # used to authorize and authenticate towards the other end-points of the API.

        headers = {
                'Authorization': 'Basic {Authorization}'.format(Authorization=Authorization),
                'Ocp-Apim-Subscription-Key': '{subscribtion_key}'.format(subscribtion_key=self.subscribtion_key),
            }
        token = requests.post('https://proxy.momoapi.mtn.com/collection/token/', headers=headers)
        #token.status_code
        token = token.json()
        token = token['access_token']
        self.token = token

    def send_request(self, amount, currency, partyId):
        self.get_token()
        self.x_reference_id= str(uuid.uuid4()) # UUID 4 Generator
        #print(self.x_reference_id)
        apikey = self.api_key
        token = self.token
        # requesttopay - POST
        # This operation is used to request a payment from a consumer (Payer).
        # The payer will be asked to authorize the payment. 
        # The transaction will be executed once the payer has authorized the payment. 
        # The requesttopay will be in status PENDING until the transaction is authorized or declined by the payer or it is timed out by the system. 
        # Status of the transaction can be validated by using the GET /requesttopay/<resourceId>

        url = 'https://proxy.momoapi.mtn.com/collection/v1_0/requesttopay'
        request_body = {
            "amount": amount,
            "currency": "XOF",
            "externalId": self.x_reference_id,
            "payer": {
                "partyIdType": "MSISDN",
                "partyId": str(partyId)
            },
            "payerMessage": "Test de payement MTN Mobile Money",
            "payeeNote": "Test de payement MTN Mobile Money"
        }

        payload=json.dumps(request_body)
    
            
        headers = {
                'Authorization': 'Bearer {token}'.format(token=token),
                'X-Reference-Id': '{x_reference_id}'.format(x_reference_id=self.x_reference_id),
                'X-Target-Environment': 'mtnbenin',
                'Ocp-Apim-Subscription-Key': '{subscribtion_key}'.format(subscribtion_key=self.subscribtion_key),
                'Content-Type': 'application/json'
        }
            
        requesttopay = requests.post(url, headers=headers, data=(payload))
        return requesttopay.status_code

    def get_request(self):
        apikey = self.api_key
        token = self.token
        url = 'https://proxy.momoapi.mtn.com/collection/v1_0/requesttopay/'+self.x_reference_id
            
        headers = {
                'Authorization': 'Bearer {token}'.format(token=token),
                'X-Reference-Id': '{x_reference_id}'.format(x_reference_id=self.x_reference_id),
                'X-Target-Environment': 'mtnbenin',
                'Ocp-Apim-Subscription-Key': '{subscribtion_key}'.format(subscribtion_key=self.subscribtion_key),
                'Content-Type': 'application/json'
        }
            
        requesttopay = requests.get(url, headers=headers)
        content = requesttopay.json()
        print(content)

        if content["status"] == "SUCCESSFUL":
            return True
        elif content["status"] == "PENDING":
            return False


    def payment(self, amount, currency, partyId):
        paiement = self.send_request(amount, currency, partyId)

        for i in range(0,30):
            time.sleep(1)
            result = self.get_request()
            if result:
                return True
                break
        
        return False
