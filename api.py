from flask import Flask
from flask_cors import CORS
from flask import request, jsonify
from send_request import SendRequest
import time


app = Flask(__name__)
obj = SendRequest()
 

app.config['CORS_HEADERS'] = 'Content-Type'

CORS(app, resources={
    r'/*': {
        'origins': '*'
    }
})

#Executer api paiement mtn

@app.route('/payment_request', methods=['POST'])
def get_all():
    data = request.get_json()
    result = obj.payment(data["amount"], data["currency"], data["partyId"])
    if result:
        return jsonify({"message":"Requete terminée"}), 200
    else:
        return jsonify({"message":"Echec de la requête"}), 500



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port="8580")